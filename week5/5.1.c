int g(int x)
{
	return x + 8;
}

int f(int x)
{
	return g(x);
}

int main(void)
{
	return f(8)+1;
}
