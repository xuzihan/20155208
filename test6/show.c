#include<stdio.h>
typedef unsigned char *byte_pointer;
void show_bytes(byte_pointer start,size_t len){
	size_t i;
	for(i=0;i < len;i++)
	printf(" %.2x",start[i]);
	printf("\n");
	}
void show_int(int x) {
	show_bytes((byte_pointer) &x,sizeof(int));
	}
void show_float(float x) {
	show_bytes((byte_pointer) &x,sizeof(float));
	}
void show_pointer(void *x) {
	show_bytes((byte_pointer) &x,sizeof(void *));
	} 
	
int main()
        {
        int i=0x11223344;
        char *p;

        p = (char *)&i;
        if (*p==0x440){
        printf("20155208 pc is Little endian\n");
        }
        else {
        printf("20155208 pc is Big endian\n");
        }return 0;
}

